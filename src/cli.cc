#include <iostream>
#include "cli.h"

DEFINE_uint64(duration, 6000, "Duration playlist");
DEFINE_string(genre, "", "Music's Genre");
DEFINE_string(type, "", "Playlist's Format");
DEFINE_string(name, "", "Music's title");
DEFINE_string(playlist, "", "Playlist's name");
DEFINE_string(subgenre, "", "Music's subgenre");
DEFINE_string(artist, "", "Music's Artist");
DEFINE_string(album, "", "Album's name");
