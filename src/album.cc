#include <iostream>
#include "album.h"

Album::Album():
  id(0),
  date(date),
  name("")
{}

Album::Album(unsigned int _id,
             std::chrono::system_clock _date,
             std::string _name) :
  id(_id),
  date(_date),
  name(_name)
{}

unsigned int Album::get_id()
{
  return id;
}

std::chrono::system_clock Album::get_date()
{
  return date;
}

std::string Album::get_name()
{
  return name;
}
