#include <iostream>
#include "polyphony.h"

Polyphony::Polyphony() :
  id(0),
  number(0)
{}

Polyphony::Polyphony(unsigned int _id,
                     unsigned short int _number) :
  id(_id),
  number(_number)
{}

unsigned int Polyphony::get_id()
{
  return id;
}

unsigned short int Polyphony::get_number()
{
  return number;
}

