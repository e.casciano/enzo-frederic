#include <iostream>
#include <fstream>
#include <cstring>
#include <libpq-fe.h>
#include "playlist.h"
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>

Playlist::Playlist() :
  id(0),
  title("playlist"),
  duration(0),
  tracks()
{}

Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track> _tracks) :

  id(_id),
  title(_title),
  duration(_duration),
  tracks(_tracks)
{}

void Playlist::set_duration(std::chrono::duration<int> _duration)
{
  duration = _duration;
}

void Playlist::set_track(std::vector<Track> _tracks)
{
  tracks = _tracks;
}

unsigned int Playlist::get_id()
{
  return id;
}

std::string Playlist::get_title()
{
  return title;
}
std::chrono::duration<int> Playlist::get_duration()
{
  return duration;
}

std::vector<Track> Playlist::get_track()
{
  return tracks;
}

void Playlist::writeXSPF(std::string requete, unsigned int duree_demandee)
{
  int code_retour = 0;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=e.casciano password=P@ssword";
  PGconn *connexion;
  char *reponse_requete;
  connexion = PQconnectdb(info_connexion);
  PGresult *resultat = PQexec(connexion, requete.c_str());
  Xspf::XspfIndentFormatter formatter;
  XML_Char const *const baseUri = _PT("http://radio6mic.net/");
  Xspf::XspfWriter *const writer = Xspf::XspfWriter::makeWriter(formatter, baseUri);
  Xspf::XspfTrack *track;
  srand(time(NULL));
  unsigned int temps_reel = 0;

  while(temps_reel <= duree_demandee)
  {
    int j = rand() % PQntuples(resultat);
    track = new Xspf::XspfTrack();
    char *name = PQgetvalue(resultat, j, 2);
    char *path = PQgetvalue(resultat, j, 3);
    char *artist = PQgetvalue(resultat, j, 11);
    char *album = PQgetvalue(resultat, j, 16);
    unsigned int duration = atoi(PQgetvalue(resultat, j, 1));
    track->lendTitle(_PT(name));
    track->lendAppendLocation(_PT(path));
    track->lendCreator(_PT(artist));
    track->lendAlbum(_PT(album));
    temps_reel += duration;

    if(temps_reel < duree_demandee)
    {
      writer->addTrack(track);
    }
  }

  writer->writeFile(_PT("playlist.xspf"));
}

void Playlist::writeM3U(std::string requete, unsigned int duree_demandee)
{
  int code_retour = 0;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=Cours user=e.casciano password=P@ssword";
  PGPing code_retour_ping = PQping(info_connexion);
  PGconn *connexion;
  Xspf::XspfIndentFormatter formatter;
  char *reponse_requete;
  connexion = PQconnectdb(info_connexion);
  PGresult *resultat = PQexec(connexion, requete.c_str());
  unsigned int temps_reel = 0;
  std::string name = title + ".m3u";
  std::ofstream m3u_file;
  m3u_file.open(name, std::ios::out);
  srand(time(NULL));

  while(temps_reel <= duree_demandee)
  {
    int j = rand() % PQntuples(resultat);
    unsigned int duration = atoi(PQgetvalue(resultat, j, 1));
    temps_reel += duration;

    if(temps_reel < duree_demandee)
    {
      m3u_file.write(PQgetvalue(resultat, j, 3), strlen(PQgetvalue(resultat, j, 3)));
      m3u_file.write("\n", strlen("\n"));
    }
  }

  m3u_file.close();
}
