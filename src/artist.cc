#include <iostream>
#include "artist.h"

Artist::Artist() :
  id(0),
  name("")
{}

Artist::Artist(unsigned int _id,
               std::string _name) :
  id(_id),
  name(_name)
{}

unsigned int Artist::get_id()
{
  return id;
}

std::string Artist::get_name()
{
  return name;
}

