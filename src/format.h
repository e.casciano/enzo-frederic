#ifndef FORMAT_H
#define FORMAT_H
#include <string>

/**
 * \class Format
 * \brief Definition of the Format class
 */
class Format
{
 public:

  /**
   * \brief Default constructor of Format
   */
  Format();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of a format
   * \param[in] _titule the titule of a format
   */
  Format(unsigned int _id, std::string _titule);

  /**
   * \brief Lets you know the id of a format
   * \return unsigned int representing the id of a format
   */
  unsigned int get_id();




  /**
   * \brief Lets you know the titule of a format
   * \return std::string representing the titule of a format
   */
  std::string get_titule();

  /**
   * \brief Lets you change the titule of a format
   * \param[in] _titule the titule of a format
   */
  void set_titule(std::string _titule);

 private:
  unsigned int id; ///< Attribute defining the id of the format
  std::string titule; ///< Attribute defining the titule of the format
};
#endif
