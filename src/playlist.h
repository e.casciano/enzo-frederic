#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <string>
#include <chrono>
#include <vector>
#include "track.h"
#include <libpq-fe.h>
#include <iostream>
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>
#include <stdlib.h>
#include <time.h>

/**
 * \class Playlist
 * \brief Definition of the Playlist class
 */
class Playlist
{
 public:
  /**
   * \brief Default constructor of Playlist
   */
  Playlist();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of a playlist
   * \param[in] _title the title of a playlist
   * \param[in] _duration the duration of a playlist
   * \param[in] _tracks Tracks in the playlist
   */
  Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track> _tracks);

  /**
   * \brief Lets you know the id of a playlist
   * \return unsigned int representing the id of a playlist
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the title of a playist
   * \return std::string representing the title of a playlist
   */
  std::string get_title();

  /**
   * \brief Lets you know the duration of a playlist
   * \return std::chrono::duration<int> representing the duration of a playlist
   */
  std::chrono::duration<int> get_duration();

  /**
   * \brief Lets you know a track of a playlist
   * \return std::vector<Track> representing a track of a playlist
   */
  std::vector<Track> get_track();
  
  /**
   * \brief Lets you change the duration of the playlist
   * \param[in] _duration representing the duration of a playlist
   */
  void set_duration(std::chrono::duration<int> _duration);

  /**
   * \brief Lets you change tracks of the playlist
   * \param[in] _tracks representing tracks of a playlist
   */
  void set_track(std::vector<Track> _tracks);

  /**
   * \brief Create a xspf file containing the playlist
   * \param[in] requete representing the request to recover the necessary informations
   * \param[in] duree_demandee representing the duration of the playlist requested by the user
   */
  void writeXSPF(std::string requete, unsigned int duree_demandee);

  /**
   * \brief Create a m3u file containing the playlist
   * \param[in] requete representing the request to recover the necessary informations
   * \param[in] duree_demandee representing the duration of the playlist requested by the user
   */
  void writeM3U(std::string requete, unsigned int duree_demandee);

 private:
  unsigned int id; ///< Attribute defining the id of the playlist
  std::string title; ///< Attribute defining the title of the playlist
  std::chrono::duration<int> duration; ///< Attribute defining the duration of the playlist
  std::vector<Track> tracks; ///< Attribute defining tracks of the playlist
};
#endif
