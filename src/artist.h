#ifndef ARTIST_H
#define ARTIST_H
#include <string>

/**
 * \class Artist
 * \brief Definition of the Artist class
 */
class Artist
{

 public:

  /**
   * \brief Default constructor of Artist
   */
  Artist();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of an artist
   * \param[in] _name the name of an artist
   */
  Artist(unsigned int _id, std::string _name);

  /**
   * \brief Lets you know the id of an artist
   * \return unsigned int representing the id of an artist
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the name of an artist
   * \return std::string representing the name of an artist
   */
  std::string get_name();

 private:
  unsigned int id; ///< Attribute defining the id of the artist
  std::string name; ///< Attribute determining the name of the artist
};
#endif
